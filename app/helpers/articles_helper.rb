module ArticlesHelper
  def persisted_comments(comments)
    #we only want that are coming from the database
    comments.reject{ |comment| comment.new_record?}
  end
end
