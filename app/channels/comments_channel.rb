class CommentsChannel < ApplicationCable::Channel
  def subscribed
    # stream_from "some_channel" default generated
    stream_from "comments"
    #next step: broadcast to the channel, where is comments created
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
